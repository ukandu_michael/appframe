<?php
return [
    
    'site_name'=> 'Appframe',
    'site_email'=> 'vcordukandu@gmail.com',
    'site_description'=> 'The Number One Laravel CMF for lazy Programmers.',
    'site_url'=> 'http://127.0.0.1/final/',
    'site_phone'=> '07061244001',
    'facebook_link'=> 'http://facebook.com/UkanduCMF',
    'twitter_link'=> 'http://twitter.com/UkanduCMF',
	'generator'=> 'ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijklmnpqrstuvwxyz123456789'
];