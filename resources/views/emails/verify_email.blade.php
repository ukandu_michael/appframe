@extends('emails.layout')

@section('title', 'Congratulations!')

@section('content')
<h5 class="text-left" style="text-align: left; color: #767676; font-family: Helvetica, Arial, sans-serif; font-weight: normal; line-height: 1.3; word-wrap: normal; font-size: 20px; margin: 0 0 10px; padding: 0;" align="left">Hi {{$user->username}}!</h5>
<p class="text-left" style="text-align: left; color: #767676; font-family: Helvetica, Arial, sans-serif; font-weight: normal; line-height: 1.3; font-size: 16px; margin: 0 0 10px; padding: 0;" align="left">Your Registration on {{config('site.site_name')}} is almost complete,</p>
<p class="text-left" style="text-align: left; color: #767676; font-family: Helvetica, Arial, sans-serif; font-weight: normal; line-height: 1.3; font-size: 16px; margin: 0 0 10px; padding: 0;" align="left">Confirm your email address by clicking the button below:</p>

<table class="button radius custom small-expanded" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: auto; margin: 0 0 16px; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="word-wrap: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; line-height: 1.3; font-size: 16px; margin: 0; padding: 0;" align="left" valign="top">
<table style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="word-wrap: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #fefefe; font-family: Helvetica, Arial, sans-serif; font-weight: normal; line-height: 1.3; font-size: 16px; border-radius: 3px; background: #DC4A01; margin: 0; padding: 0; border: 0px solid #ec5840;" align="left" bgcolor="#DC4A01" valign="top"><a href="{{route('verify', ['token'=> $user->email_token])}}" style="color: #fefefe; font-family: Helvetica, Arial, sans-serif; font-weight: bold; text-align: left; line-height: 1.3; text-decoration: none; font-size: 16px; display: inline-block; border-radius: 3px; margin: 0; padding: 8px 16px; border: 0 solid #ec5840;">Confirm Now</a></td>
</tr></table></td>
</tr></table>
@endsection