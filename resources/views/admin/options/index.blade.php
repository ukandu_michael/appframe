@extends('admin.layouts.base')

@section('title', 'Bulding Faster')

@section('content')
  
<section class="content">

<div class="row">

<div class="col-md-6">

<div class="box box-info">
<div class="box-header with-border">
<h3 class="box-title">Basic Setting</h3>
</div>
<div class="box-body">

<form action="{{route('admin_options_update_basic')}}" method="POST" data-ajax name="UpdateBasicConfig">
{{ csrf_field() }}

<div class="form-group site_name">
<label for="username" class="control-label">Site Name</label>
<input type="text" class="form-control" name="site_name" placeholder="Site Name" required data-minlength="6" value="{{ $utils->option('site_name') }}">
<div class="help-block with-errors"></div>
</div>

<div class="form-group site_description">
<label for="username" class="control-label">Site Description</label>
<textarea class="form-control" name="site_description">{{ $utils->option('site_description') }}</textarea>
<div class="help-block with-errors"></div>
</div>

<div class="form-group site_url">
<label for="username" class="control-label">Site Link</label>
<input type="text" class="form-control" name="site_url" placeholder="Site Link" required data-minlength="6" value="{{ $utils->option('site_url') }}" disabled="disabled">
<div class="help-block with-errors"></div>
</div>

<div class="form-group site_metakeywords">
<label for="username" class="control-label">Meta Keywords</label>
<textarea class="form-control" name="site_metakeywords">{{ $utils->option('site_metakeywords') }}</textarea>
<div class="help-block with-errors"></div>
</div>

<div class="form-group site_metadescription">
<label for="username" class="control-label">Meta Description</label>
<textarea class="form-control" name="site_metadescription">{{ $utils->option('site_metadescription') }}</textarea>
<div class="help-block with-errors"></div>
</div>

<div class="form-group">
<button type="submit" class="btn btn-success" id="UpdateBasicConfig">Save Changes</button>
</div>

</form>

</div>

</div>
<!-- /.box -->
</div>

<div class="col-md-6">

<div class="box box-info">
<div class="box-header with-border">
<h3 class="box-title">Mail Setting</h3>
</div>
<div class="box-body">
<form action="{{route('admin_options_update_mail')}}" method="POST" data-ajax name="UpdateMailConfig">
{{ csrf_field() }}

<div class="form-group mail_server">
<label for="username" class="control-label">Mail Server</label>
<input type="text" class="form-control" name="mail_server" placeholder="Mail Server" value="{{ $utils->option('mail_server') }}">
<div class="help-block with-errors"></div>
</div>

<div class="form-group mail_login">
<label for="username" class="control-label">Mail Login</label>
<input type="text" class="form-control" name="mail_login" placeholder="Mail Server" value="{{ $utils->option('mail_login') }}">
<div class="help-block with-errors"></div>
</div>

<div class="form-group mail_password">
<label for="username" class="control-label">Mail Password</label>
<input type="text" class="form-control" name="mail_password" placeholder="Mail Password" value="{{ $utils->option('mail_password') }}">
<div class="help-block with-errors"></div>
</div>

<div class="form-group mail_port">
<label for="username" class="control-label">Mail Port</label>
<input type="text" class="form-control" name="mail_port" placeholder="Mail Port" value="{{ $utils->option('mail_port') }}">
<div class="help-block with-errors"></div>
</div>

<div class="form-group mail_driver">
<label for="username" class="control-label">Mail Driver</label>
<input type="text" class="form-control" name="mail_driver" placeholder="Mail Driver" value="{{ $utils->option('mail_driver') }}">
<div class="help-block with-errors"></div>
</div>

<div class="form-group mail_encryption">
<label for="username" class="control-label">Mail Encryption</label>
<select class="form-control" name="mail_encryption">
  <option value="tls" {{ $utils->option('mail_encryption') == 'tls' ? 'selected' : '' }}>TLS</option>
  <option value="ssl" {{ $utils->option('mail_encryption') == 'ssl' ? 'selected' : '' }}>SSL</option>
</select>
<div class="help-block with-errors"></div>
</div>

<div class="form-group">
<button type="submit" class="btn btn-success" id="UpdateMailConfig">Save Changes</button>
</div>

</form>
</div>

</div>
<!-- /.box -->
</div>

<div class="col-md-6">

<div class="box box-info">
<div class="box-header with-border">
<h3 class="box-title">Theme Setting</h3>
</div>
<div class="box-body">
<form action="{{route('admin_options_update_theme')}}" method="POST" data-ajax name="UpdateThemeConfig">
{{ csrf_field() }}

<div class="form-group theme">
<label for="username" class="control-label">Select Theme</label>
<select class="form-control" name="theme">
  @foreach($themes as $theme)
  <option value="{{ $theme['dir'] }}" {{ $utils->option('theme') == $theme['dir'] ? 'selected' : '' }} >{{ $theme['name'] }}</option>
  @endforeach
 </select>
<div class="help-block with-errors"></div>
</div>

<div class="form-group">
<button type="submit" class="btn btn-success" id="UpdateThemeConfig">Save Changes</button>
</div>

</form>
</div>

</div>
<!-- /.box -->
</div>



</div>

</section>
	
@endsection 