<div class="container" id="message">
<div class="row">
<div class="col-md-8 col-md-offset-2">
@if (session('success'))
<div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<p>{{ session('success') }}</p>
</div>
@endif

@if (session('error'))
<div class="alert alert-danger alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<p>{{ session('error') }}</p>
</div>
@endif

</div>
</div>
</div>