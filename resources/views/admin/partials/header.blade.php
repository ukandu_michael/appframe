<header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>{{ substr($utils->option('site_name'), 0,2) }}</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>{{ $utils->option('site_name') }}</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{!Auth::user()->photo_url ? asset('admin/images/avatar.png') : Auth::user()->photo_url}}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{Auth::user()->username}} {{Auth::user()->lastname}}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{!Auth::user()->photo_url ? asset('admin/images/avatar.png') : Auth::user()->photo_url}}" class="img-circle" alt="User Image">

                <p>
                  {{Auth::user()->username}} {{Auth::user()->lastname}} - {{Auth::user()->occupation}}
                  <small>Member since {{Auth::user()->created_at->toFormattedDateString()}}</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="{{route('home')}}" class="btn btn-default btn-flat" target="_blank">Go To Site</a>
                </div>
                <div class="pull-right">
                 <a href="{{route('admin_logout')}}" class="btn btn-default btn-flat" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Sign Out</a>
				  <form id="logout-form" action="{{ route('admin_logout') }}" method="POST" style="display: none;">
					{{ csrf_field() }}
					</form>
                </div>
              </li>
            </ul>
          </li>
            
          <!-- Control Sidebar Toggle Button -->
            
          <li>
            <a href="{{route('home')}}" target="_blank">Visit Site <i class="glyphicon glyphicon-arrow-right"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
     
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">DASHBOARD</li>
        <li>
          <a href="{{route('admin')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        
         <li class="header">USER MANAGEMENT</li>
        <li class="treeview">
          <a href="#">
            <i class="ion-ios-people fa-lg"></i>
            <span>User Manager</span>
               <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> 
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('admin_user')}}"><i class="fa fa-circle-o"></i>All Users</a></li>
            <li><a href="{{route('admin_role')}}"><i class="fa fa-circle-o"></i>Role Manager</a></li>
            <li><a href="{{route('admin_add_role')}}"><i class="fa fa-circle-o"></i>Add Role</a></li>
          </ul>
        </li>
       <li class="header">SITE CONFIRGURATION</li>
	    <li>
          <a href="{{route('admin_options')}}">
            <i class="fa fa-cog"></i> <span>Configurations</span>
          </a>
        </li>
      
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
