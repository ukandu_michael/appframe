<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> AppFrame 1.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="#">Appframe</a>.</strong> All rights
    reserved.
  </footer>
 