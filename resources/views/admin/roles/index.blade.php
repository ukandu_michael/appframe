@extends('admin.layouts.base')

@section('title', 'Role Manager')

@section('content')
  
<section class="content">

	<div class="row">
	<div class="col-xs-12">
	<!-- Default box -->
	<div class="box box-info">
	<div class="box-header with-border">
	<h3 class="box-title">Role Management</h3>
	<span class="pull-right"><a href="{{route('admin_add_role')}}" class="btn btn-success"><i class="fa fa-plus fa-lg"></i> Add Role</a></span>
	</div>
	<div class="box-body">

	<div class="table-responsive">
	<table id="example1" class="table table-bordered table-condensed table-hover">
	<thead>
	<tr>
	<th>Name</th>
	<th>Permissions</th>
	<th>Actions</th>
	</tr>
	</thead>

	<tbody>
   @foreach($roles as $role)
	<tr>
	<td>{{$role->name}}</td>
	<td>{{implode(', ', json_decode($role->permissions, true))}}</td>
	<td>
    <a href="{{route('admin_edit_role', ['id'=> $role->id])}}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Edit Role"><i class="fa fa-edit fa-lg"></i></a>
	<a href="{{route('admin_delete_role', ['id'=> $role->id])}}" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Delete Role"><i class="fa fa-trash fa-lg"></i></a>
    @if(!$role->is_default)
	<a href="{{route('admin_default_role', ['id'=> $role->id, 'action'=> 'add'])}}" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Make Default Role"><i class="fa fa-refresh fa-lg"></i></a>
    @else
	<a href="{{route('admin_default_role', ['id'=> $role->id, 'action'=> 'remove'])}}" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Remove Default Role"><i class="fa fa-ban fa-lg"></i></a>
    @endif
    </td>
	</tr>
   @endforeach
	</tbody>

	</table>

	</div>
	{{ $roles->links() }}
	</div>
	
	</div>
	<!-- /.box -->
	</div>
	</div>

	</section>
	
@endsection 