@extends('admin.layouts.base')

@section('title', 'Add Role')

@section('content')

<!-- Content Header (Page header) -->
<section class="content">
<div class="row">
<div class="col-md-8 col-md-offset-2">

<div class="box box-success">
<div class="box-header with-border">
<h3 class="box-title">Edit Role : " {{$role->name}} "</h3>
</div>
<div class="box-body">

<form action="{{route('admin_edit_role', ['id'=> $role->id])}}" method="POST" data-ajax name="AddRole">
{{ csrf_field() }}
<div class="form-group name">
<label for="provider" class="control-label">Name</label>
<input type="text" class="form-control" name="name" placeholder="Role Name" value="{{$role->name}}">
<div class="help-block with-errors"></div>
</div>
<div class="checkbox is_default">
<label>
<input type="checkbox" name="is_default" value="1" @if($role->is_default) checked @else '' @endif> Make Default
</label>
<div class="help-block with-errors"></div>
</div>

<div class="form-group permissions[]">
<label for="provider" class="control-label">Set Permissions</label>
<select multiple class="form-control selectpicker" name="permissions[]" data-live-search="true">
@foreach($permissions as $perm)
<option value="{{$perm->name}}" @if($perm->status) selected @else '' @endif>{{$perm->name}}</option>
@endforeach
</select>
<div class="help-block with-errors"></div>
</div>


<div class="form-group">
<div>
<button type="button" class="btn btn-success" id="AddRole">Add Role</button>
</div>
</div>

</form>


</div>
</div>

</div>
</div>

</section>

@endsection