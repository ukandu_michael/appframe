@extends('admin.layouts.base')

@section('title', 'Add Role')

@section('content')

<!-- Content Header (Page header) -->
<section class="content">
<div class="row">
<div class="col-md-8 col-md-offset-2">

<div class="box box-success">
<div class="box-header with-border">
<h3 class="box-title">Add Role</h3>
</div>
<div class="box-body">

<form action="{{route('admin_add_role')}}" method="POST" data-ajax name="AdminAddRole">
{{ csrf_field() }}
<div class="form-group name">
<label for="provider" class="control-label">Name</label>
<input type="text" class="form-control" name="name" placeholder="Role Name" required>
<div class="help-block with-errors"></div>
</div>
<div class="checkbox is_default">
<label>
<input type="checkbox" name="is_default" value="1"> Make Default
</label>
<div class="help-block with-errors"></div>
</div>

<div class="form-group permissions[]">
<label for="provider" class="control-label">Set Permissions</label>
<select multiple class="form-control selectpicker" name="permissions[]" data-live-search="true">
@foreach($permissions as $permission)
<option value="{{$permission}}">{{$permission}}</option>
@endforeach
</select>
<div class="help-block with-errors"></div>
</div>

<div class="form-group">
<div>
<button type="submit" class="btn btn-success" id="AdminAddRole">Add Role</button>
</div>
</div>

</form>


</div>
</div>

</div>
</div>

</section>

@endsection