@extends('admin.layouts.login')

@section('title', 'Bulding Faster')

@section('content')
   <div class="login-box-body">
    <p class="login-box-msg"></p>

    <form action="{{route('admin_login')}}" method="post" data-ajax name="AdminLoginForm">
	{{ csrf_field() }}
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" name="email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		<div class="help-block with-errors"></div>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
		<div class="help-block with-errors"></div>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="remember" value="1"> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="button" class="btn btn-primary btn-block btn-flat" id="AdminLoginForm">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <a href="#">Forgot Password?</a><br>
  </div>
<br><br>
  <!-- /.login-box-body -->
<div class="text-center">
<a href="{{route('home')}}" class="btn btn-danger btn-lg"><i class="glyphicon glyphicon-arrow-left"></i> Go Back Home</a>
</div>
	 
@endsection