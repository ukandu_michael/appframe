@extends('admin.layouts.base')

@section('title', 'All Users')

@section('content')

<!-- Content Header (Page header) -->
<section class="content">
 <div class="row">
<div class="col-md-8 col-md-offset-2">

<div class="box box-info">
<div class="box-header with-border">
<h3 class="box-title">Add User</h3>
</div>
<div class="box-body">
    
<form action="{{route('admin_add_user')}}" method="POST" data-ajax name="AddUserForm">
{{ csrf_field() }}

<div class="input-group">
   <span class="input-group-btn">
     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary APP_UPLOADER">
       <i class="fa fa-picture-o"></i> Choose
     </a>
   </span>
   <input id="thumbnail" class="form-control" type="text" name="photo_url">
 </div>
 <img id="holder" style="margin-top:15px;max-height:100px;">
     
<div class="form-group username">
<label for="username" class="control-label">Username</label>
<input type="text" class="form-control" name="username" placeholder="Username" required data-minlength="6">
<div class="help-block with-errors"></div>
</div>

<div class="form-group email">
<label for="email" class="control-label">Email</label>
<input type="email" class="form-control" name="email" placeholder="Email" required data-minlength="6">
<div class="help-block with-errors"></div>
</div>

<div class="form-group password">
<label for="password" class="control-label">Password</label>
<input type="password" class="form-control" name="password" placeholder="Password" id="password" required data-minlength="6">
<div class="help-block with-errors"></div>
</div>

<div class="form-group confirm_password">
<label for="confirm_password" class="control-label">Confirm Password</label>
<input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" data-match="#password" data-match-error="passwords do not match" required>
<div class="help-block with-errors"></div>
</div>

<div class="form-group lastname">
<label for="lastname" class="control-label">Last Name</label>
<input type="text" class="form-control" name="lastname" placeholder="Last Name" data-minlength="6">
<div class="help-block with-errors"></div>
</div>

<div class="form-group firstname">
<label for="firstname" class="control-label">First Name</label>
<input type="text" class="form-control" name="firstname" placeholder="First Name" data-minlength="6">
<div class="help-block with-errors"></div>
</div>

<div class="form-group is_admin">
<div class="checkbox">
<label>
  <input type="checkbox" name="is_admin" value="1"> Make Admin
</label>
</div>
<div class="help-block with-errors"></div>
</div>

<div class="form-group role">
<label for="role" class="control-label">Select Role</label>
<select class="form-control selectpicker" name="role" data-live-search="true">
@foreach($roles as $role)
<option value="{{$role->id}}">{{$role->name}}</option>
@endforeach
</select>
<div class="help-block with-errors"></div>
</div>

<button type="submit" class="btn btn-success" id="AddUserForm">Create</button>
</form>


</div>
</div>

</div>
</div>

</section>

@endsection