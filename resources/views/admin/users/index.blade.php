@extends('admin.layouts.base')

@section('title', 'All Users')

@section('content')
  
<section class="content">

	<div class="row">
	<div class="col-xs-12">
	<!-- Default box -->
	<div class="box box-info">
	<div class="box-header with-border">
	<h3 class="box-title">User Management</h3>
	<span class="pull-right"><a href="{{route('admin_add_user')}}" class="btn btn-success"><i class="fa fa-plus fa-lg"></i> Add User</a></span>
	<hr>
	<form class="form-inline" action="{{ route('admin_search', ['route_name'=> 'admin_user']) }}" method="post">
		{{csrf_field()}}
	<div class="form-group">
	<label>Search</label>
	<input type="text" name="search_term[q]" class="form-control" placeholder="Search By Username, Email or Ref Number">
	</div>
	
	<div class="input-group date">
	<div class="input-group-addon">
	<i class="fa fa-calendar"></i>
	</div>
	<input type="text" class="form-control pull-right datepicker" placeholder="From: " name="search_term[date_from]">
	</div>
	
	<div class="input-group date">
	<div class="input-group-addon">
	<i class="fa fa-calendar"></i>
	</div>
	<input type="text" class="form-control pull-right datepicker" placeholder="To: " name="search_term[date_to]">
	</div>
	
	<div class="form-group">
	<button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Search</button>
	</div>
	
	</form>
	</div>
	<div class="box-body">

	<div class="table-responsive">
	<table id="example1" class="table table-bordered table-condensed table-hover">
	<thead>
	<tr>
	<th></th>
	<th>Username</th>
	<th>Ref</th>
	<th>Email</th>
	<th>Actions</th>
	</tr>
	</thead>

	<tbody>
   
    @foreach($users as $user)
	<tr>
	<td><img src="{{!Auth::user()->photo_url ? asset('admin/images/avatar.png') : Auth::user()->photo_url}}" class="img-responsive" width="40"></td>
	<td>{{$user->username}}</td>
	<td>{{$user->ref}}</td>
	<td>{{$user->email}}</td>
	<td>
    <a href="{{route('admin_edit_user', ['ref'=> $user->ref])}}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Edit User"><i class="fa fa-edit fa-lg"></i></a>
	<a href="{{route('admin_delete_user', ['ref'=> $user->ref])}}" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Delete User"><i class="fa fa-trash fa-lg"></i></a>
    @if(!$user->is_active)
	<a href="{{route('admin_user_activate_toggle', ['ref'=> $user->ref, 'action'=>'activate'])}}" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Activate User"><i class="fa fa-refresh fa-lg"></i></a>
    @else
	<a href="{{route('admin_user_activate_toggle', ['ref'=> $user->ref, 'action'=>'deactivate'])}}" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="Deactivate User"><i class="fa fa-ban fa-lg"></i></a>
    @endif
    <a href="{{route('admin_edit_user_password', ['ref'=> $user->ref])}}" class="btn btn-sm bg-olive" data-toggle="tooltip" data-placement="top" title="Edit User Password"><i class="fa fa-lock fa-lg"></i></a>
    </td>
	</tr>
   @endforeach
        
	</tbody>

	</table>

	</div>
	{{ $users->links() }}
	</div>

	
	</div>
	<!-- /.box -->
	</div>
	</div>

	</section>
	
@endsection 