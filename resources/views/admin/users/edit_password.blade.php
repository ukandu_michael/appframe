@extends('admin.layouts.base')

@section('title', 'Edit Users')

@section('content')

<!-- Content Header (Page header) -->
<section class="content">
 <div class="row">
<div class="col-md-8 col-md-offset-2">

<div class="box box-success">
<div class="box-header with-border">
<h3 class="box-title">Edit User " {{$user->username}} "</h3>
</div>
<div class="box-body">
    
<form action="{{route('admin_edit_user_password', ['ref'=> $user->ref])}}" method="POST" data-ajax data-toggle="validator">
{{ csrf_field() }}

<div class="form-group">
<label for="password" class="control-label">Old Password</label>
<input type="password" class="form-control" name="old_password" placeholder="Old Password" required data-minlength="6">
<div class="help-block with-errors"></div>
</div>
    
<div class="form-group">
<label for="password" class="control-label">New Password</label>
<input type="password" class="form-control" name="password" placeholder="New Password" id="password" required data-minlength="6">
<div class="help-block with-errors"></div>
</div>

<div class="form-group">
<label for="confirm_password" class="control-label">Confirm Password</label>
<input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" data-match="#password" data-match-error="passwords do not match" required>
<div class="help-block with-errors"></div>
</div>

<button type="submit" class="btn btn-success">Change Password</button>
</form>


</div>
</div>

</div>
</div>

</section>

@endsection