@extends('admin.layouts.base')

@section('title', 'Edit Users')

@section('content')

<!-- Content Header (Page header) -->
<section class="content">
 <div class="row">
<div class="col-md-8 col-md-offset-2">

<div class="box box-success">
<div class="box-header with-border">
<h3 class="box-title">Edit User " {{$user->username}} "</h3>
</div>
<div class="box-body">

<form action="{{route('admin_edit_user', ['ref'=> $user->ref])}}" method="POST" data-ajax name="EditUserForm">
{{ csrf_field() }}

<div class="input-group">
   <span class="input-group-btn">
     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary APP_UPLOADER">
       <i class="fa fa-picture-o"></i> Choose
     </a>
   </span>
   <input id="thumbnail" class="form-control" type="text" name="photo_url">
 </div>
 <img id="holder" style="margin-top:15px;max-height:100px;">
      
<div class="form-group username">
<label for="username" class="control-label">Username</label>
<input type="text" class="form-control" name="username" placeholder="Username" value="{{$user->username}}" required data-minlength="6">
<div class="help-block with-errors"></div>
</div>

<div class="form-group email">
<label for="email" class="control-label">Email</label>
<input type="email" class="form-control" name="email" placeholder="Email" value="{{$user->email}}" required data-minlength="6">
<div class="help-block with-errors"></div>
</div>


<div class="form-group lastname">
<label for="lastname" class="control-label">Last Name</label>
<input type="text" class="form-control" name="lastname" placeholder="Last Name" value="{{$user->lastname}}" data-minlength="6">
<div class="help-block with-errors"></div>
</div>

<div class="form-group firstname">
<label for="firstname" class="control-label">First Name</label>
<input type="text" class="form-control" name="firstname" placeholder="First Name" value="{{$user->firstname}}" data-minlength="6">
<div class="help-block with-errors"></div>
</div>

<div class="form-group is_admin">
<div class="checkbox">
<label>
  <input type="checkbox" name="is_admin" value="1" @if($user->is_admin) checked @endif> Make Admin
</label>
</div>
<div class="help-block with-errors"></div>
</div>

<div class="form-group role">
<label for="role" class="control-label">Select Role</label>
<select class="form-control selectpicker" name="role" data-live-search="true">
@foreach($utils::resolveRoles($user->role_id) as $role)
<option value="{{$role->id}}" @if($role->is_selected) selected @endif>{{$role->name}}</option>
@endforeach
</select>
<div class="help-block with-errors"></div>
</div>

<button type="submit" class="btn btn-success" id="EditUserForm">Create</button>
</form>


</div>
</div>

</div>
</div>

</section>

@endsection