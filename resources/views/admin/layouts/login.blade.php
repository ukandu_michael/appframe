<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Galant CMS - @yield('title')</title>

    <!-- Bootstrap -->
    <link href="{{asset('public/admin/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('public/admin/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('public/admin/css/ionicons.css')}}" rel="stylesheet">
    <link href="{{asset('public/admin/plugins/toastr/build/toastr.css')}}" rel="stylesheet">
    <link href="{{asset('public/admin/css/admin.css')}}" rel="stylesheet">
    <link href="{{asset('public/admin/plugins/iCheck/square/blue.css')}}" rel="stylesheet">
    <link href="{{asset('public/admin/css/style.css')}}" rel="stylesheet">
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-pag" spinner="{{asset('js/loading.gif')}}">
    
	
	 <div class="login-box">
	 

@if (session('success'))
 <div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 {{ session('success') }}
</div>
@elseif(session('error'))
<div class="alert alert-danger alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 {{ session('error') }}
</div>
@endif


  <div class="login-logo">
    <a href="{{route('admin_login')}}" class="text-capitalize" style="color:#fff;">AdminLogin</a>
  </div>
	
            @yield('content')
   
    </div>
    
	<script src="{{asset('public/admin/js/jquery.min.js')}}"></script>
	<script src="{{asset('public/admin/js/bootstrap.js')}}"></script>
	<script src="{{asset('public/admin/js/loadingoverlay.js')}}"></script>
	<script src="{{asset('public/admin/js/validator.js')}}"></script>
	<script src="{{asset('public/admin/plugins/toastr/build/toastr.min.js')}}"></script>
	<script src="{{asset('public/admin/plugins/iCheck/icheck.min.js')}}"></script>
	<script src="{{asset('public/admin/js/ajax-post.js')}}"></script>
	<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
  </body>
</html>