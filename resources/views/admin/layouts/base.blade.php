<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    
    <title>{{$utils->option('site_name')}} - @yield('title')</title>

	
	<link rel="stylesheet" href="{{asset('admin/css/font-awesome.css')}}" >
	<link href="{{asset('admin/css/bootstrap.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('admin/css/ionicons.css')}}">
	<link rel="stylesheet" href="{{asset('admin/css/animate.css')}}">
	<link rel="stylesheet" href="{{asset('admin/plugins/datatables/dataTables.bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('admin/plugins/file-input/css/fileinput.css')}}">
	<link rel="stylesheet" href="{{asset('admin/plugins/toastr/build/toastr.css')}}">
	<link rel="stylesheet" href="{{asset('admin/plugins/bootstrap-select/dist/css/bootstrap-select.css')}}">
	<link rel="stylesheet" href="{{asset('admin/plugins/datepicker/datepicker3.css')}}">
	<link rel="stylesheet" href="{{asset('admin/css/admin.css')}}">
	<link rel="stylesheet" href="{{asset('admin/css/_all-skins.css')}}">
	<link rel="stylesheet" href="{{asset('admin/css/app.css')}}">
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini" spinner="{{asset('admin/js/loading.gif')}}">
    
	<div class="wrapper">
	@include('admin.partials.header')
	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      @include('admin.partials.message')
      
    @yield('content')
    </div>
	
	@include('admin.partials.footer')
    </div>
     
	<script src="{{asset('admin/js/jquery.min.js')}}"></script>
	<script src="{{asset('admin/js/bootstrap.js')}}"></script>
	<script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('admin/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
	<script src="{{asset('admin/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
	<script src="{{asset('admin/plugins/fastclick/fastclick.js')}}"></script>
	<script src="{{asset('admin/plugins/file-input/js/fileinput.js')}}"></script>
	<script src="{{asset('admin/plugins/bootstrap-select/dist/js/bootstrap-select.js')}}"></script>
	<script src="{{asset('admin/js/loadingoverlay.js')}}"></script>
	<script src="{{asset('admin/js/validator.js')}}"></script>
	<script src="{{asset('admin/plugins/toastr/build/toastr.min.js')}}"></script>
	<script src="{{asset('admin/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
	<script src="{{asset('admin/js/app.js')}}"></script>
	<script src="{{asset('admin/js/upload.js')}}"></script>
	<script src="{{asset('admin/js/ajax-post.js')}}"></script>
	<script src="{{asset('/vendor/laravel-filemanager/js/lfm.js')}}"></script>
	<script src="{{asset('admin/js/admin.js')}}"></script>
	<script>
	$('.APP_UPLOADER').filemanager('image');
	
	$('.datepicker').datepicker({
      autoclose: true,
	   format: 'yyyy-mm-dd',
    });
	
	</script>
	
  </body>
</html>