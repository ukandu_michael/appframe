@extends('base.layouts.app')

@section('title', $user->username . ' Email Edit')

@section('content')
   <div class="container">
  <div class="row">
  <div class="col-md-4">
  @include('base.partials.profile_sidebar', ['user'=> $user])
  </div>
  
  <div class="col-md-8">
  
  <div class="panel panel-default">
	<div class="panel-heading">Basic Edit</div>

	<div class="panel-body">
    <form class="" action="{{route('email_edit', ['ref'=> $user->ref])}}" method="POST" data-ajax name="EmailEdit">
    {{ csrf_field() }}
	<div class="form-group email">
	<label for="email" class="control-label">Email</label>
	<input type="email" class="form-control" name="email" placeholder="Your Email" value="{{$user->email}}" >
         <div class="help-block with-errors"></div>
	</div> 
	
	<div class="form-group">
	<button type="submit" class="btn btn-info btn-block" id="EmailEdit">Save</button>
	</div>
	
	</form>
        
    
	</div>
	</div>
  
  </div>
  
   </div>
   </div>
@endsection
