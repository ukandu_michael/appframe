@extends('base.layouts.app')

@section('title', $user->username)

@section('content')
 <div class="container">
  <div class="row">
  <div class="col-md-4">
  @include('base.partials.profile_sidebar', ['user'=> $user])
  </div>
  
  <div class="col-md-8">
  {{$user->username}}
  </div>
  
   </div>
   </div>

@endsection