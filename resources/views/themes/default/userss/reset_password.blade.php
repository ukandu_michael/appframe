@extends('base.layouts.app')

@section('title', 'Reset Password')

@section('content')

<div class="container">
  <div class="row">
  <div class="col-md-4 col-md-offset-4">
  
  <div class="panel panel-default">
	<div class="panel-heading">Reset Password</div>

	<div class="panel-body">
	
	<form class="" action="{{route('reset_password', ['token'=> $token])}}" method="POST" data-ajax data-validator="validator">
    {{ csrf_field() }}
	<div class="form-group">
	<label for="password" class="control-label">Password</label>
	<input type="password" class="form-control"  name="password" placeholder="Password" id="password" required data-minlength="6">
        <div class="help-block with-errors"></div>
	</div> 
	<div class="form-group">
	<label for="confirm_password" class="control-label">Confirm Password</label>
	<input type="password" class="form-control"  name="confirm_password" placeholder="Confirm Password" data-match="#password" required data-minlength="6">
        <div class="help-block with-errors"></div>
	</div> 
	
	<div class="form-group">
	<button type="submit" class="btn btn-info btn-block">Reset Password</button>
	</div>
	
	</form>

	</div>
	
	</div>
  
  </div>
  </div>
  </div>
@endsection