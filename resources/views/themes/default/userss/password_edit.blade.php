@extends('base.layouts.app')

@section('title', $user->username . ' Password Change')

@section('content')
   <div class="container">
  <div class="row">
  <div class="col-md-4">
  @include('base.partials.profile_sidebar', ['user'=> $user])
  </div>
  
  <div class="col-md-8">
  
  <div class="panel panel-default">
	<div class="panel-heading">Password Change</div>

	<div class="panel-body">
	
    <form class="" action="{{route('password_edit', ['ref'=> $user->ref])}}" method="POST" data-ajax name="PasswordEdit">
    {{ csrf_field() }}
	<div class="form-group old_password">
	<label for="old_password" class="control-label">Old Password</label>
	<input type="password" class="form-control"  name="old_password" placeholder="Old Password">
         <div class="help-block with-errors"></div>
	</div> 
	<div class="form-group password">
	<label for="password" class="control-label">Password</label>
	<input type="password" class="form-control"  name="password" placeholder="Password" id="password" required data-minlength="6">
         <div class="help-block with-errors"></div>
	</div> 
	<div class="form-group confirm_password">
	<label for="confirm_password" class="control-label">Confirm Password</label>
	<input type="password" class="form-control"  name="confirm_password" placeholder="Confirm Password" required data-minlength="6" data-match="#password">
         <div class="help-block with-errors"></div>
	</div>  
	
	<div class="form-group">
	<button type="button" class="btn btn-info btn-block" id="PasswordEdit">Change Password</button>
	</div>
	
	</form>
        
    
	</div>
	</div>
  
  </div>
  
   </div>
   </div>
@endsection
