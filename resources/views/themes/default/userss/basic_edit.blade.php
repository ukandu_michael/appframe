@extends('base.layouts.app')

@section('title', $user->username . ' Profile Edit')

@section('content')
   <div class="container">
  <div class="row">
  <div class="col-md-4">
  @include('base.partials.profile_sidebar', ['user'=> $user])
  </div>
  
  <div class="col-md-8">
  
  <div class="panel panel-default">
	<div class="panel-heading">Basic Edit</div>

	<div class="panel-body">
    <form class="" action="{{route('basic_edit', ['ref'=> $user->ref])}}" method="POST" data-ajax  name="BasicEdit">
    {{ csrf_field() }}
	<div class="form-group username">
	<label for="email" class="control-label">Username</label>
	<input type="text" class="form-control" name="username" placeholder="Username" value="{{$user->username}}">
         <div class="help-block with-errors"></div>
	</div> 
	
	<div class="form-group">
	<button type="submit" class="btn btn-info btn-block" id="BasicEdit">Save</button>
	</div>
	
	</form>
        
    
	</div>
	</div>
  
  </div>
  
   </div>
   </div>
@endsection
