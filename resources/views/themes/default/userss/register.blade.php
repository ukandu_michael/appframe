@extends('base.layouts.app')

@section('title', 'Sign Up')

@section('content')
    <div class="container">
    <div class="row">
    <div class="col-md-4 col-md-offset-4">
    
    <div class="panel panel-default">
	<div class="panel-heading">Sign Up</div>

	<div class="panel-body">
    <form action="{{route('register')}}" role="form" method="POST" name="RegisterForm" data-ajax>
    {{ csrf_field() }} 
	<div class="form-group username">
	<label for="email" class="control-label">Username</label>
	<input type="text" class="form-control" name="username" placeholder="Username">
    <div class="help-block with-errors"></div>
	</div>
        
	<div class="form-group email">
	<label for="email" class="control-label">Email</label>
	<input type="email" class="form-control" name="email" placeholder="Email" required>
    <div class="help-block with-errors"></div>
	</div>
        
	<div class="form-group password">
	<label for="password" class="control-label">Password</label>
	<input type="password" class="form-control"  name="password" placeholder="Password" id="password" required>
    <div class="help-block with-errors"></div>
	</div> 
        
	<div class="form-group confirm_password">
	<label for="confirm_password" class="control-label">Confirm Password</label>
	<input type="password" class="form-control"  name="confirm_password" placeholder="Confirm Password" required >
    <div class="help-block with-errors"></div>
	</div> 
	
	
	<button type="button" class="btn btn-info btn-block" id="RegisterForm">Sign Up</button>
	
	
	</form>
        
    
	</div>
	</div>
        
	   
	</div>
	</div>
	</div> 
	
@endsection  