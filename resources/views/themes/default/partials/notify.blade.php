
@if (session('success'))
<p id="SUCCESSDATA" style="display:none;">{{ session('success') }}</p>
<script>
$(function(){
	toastr.success($('#SUCCESSDATA').text());
});
</script>
@elseif(session('error'))
<p id="ERRORDATA" style="display:none;">{{ session('error') }}</p>
<script>
$(function(){
	toastr.error($('#ERRORDATA').text());
});
</script>

@endif

