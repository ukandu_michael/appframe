<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{route('home')}}">Ukandu CMF</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      
      <ul class="nav navbar-nav navbar-right">
	  @if (Auth::guest())
        <li><a href="{{route('connect')}}">Login / Register</a></li>
       
	@else
		<li><a href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Sign Out</a>
				  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					{{ csrf_field() }}
					</form>
			</form>
			</li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->username }} <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{{route('profile', ['slug'=>Auth::user()->ref ])}}">Profile</a></li>
			@if(Auth::user()->is_admin)
			<li><a href="{{route('admin')}}">Admin Dashboard</a></li>
		    @endif
			<li><a href="{{ route('edit_profile', ['ref'=> Auth::user()->ref]) }}">Settings</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Sign Out</a>
				  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					{{ csrf_field() }}
					</form>
			</form>
			</li>
          </ul>
        </li>
		
	@endif 
      </ul>
	   
    </div><!-- /.navbar-collapse -->
	
  </div><!-- /.container-fluid -->
</nav>