$(function(){

$('[data-ajax]').validator().on('submit', function(e){
	if (e.isDefaultPrevented()) {
		return false;
	}else{ 
	e.preventDefault();
	$.ajax({
	url: $(this).attr('action'),
	method: $(this).attr('method'),
	data: $(this).serializeArray(),
	beforeSend: function(){
	Loader('[data-ajax]');
	},
	})
	.done(function(res){
	toastr.success(res.message, 'Success!');
	Loader('[data-ajax]', false);
	if(!res.redirect_url)
	{
		window.setTimeout(function(){
		window.location.reload();
		}, 3000);
	}else{
		return window.location.href = res.redirect_url;
	}
	
	})
	
	.fail(function(err){
	var msg = $.parseJSON(err.responseText);
	toastr.error(msg.error, 'Error!');
	Loader('[data-ajax]', false);
	});

	}
	
});

var Loader = function(el, isShow = true)
{
var url = location.protocol + '//' + location.hostname + '/galant-cms/';

if(isShow){
$(el).LoadingOverlay("show", {image:url+'admin/js/loading.gif'});
}else{
$(el).LoadingOverlay("hide");
}
}

});