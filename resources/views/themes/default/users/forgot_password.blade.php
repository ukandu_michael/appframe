@extends($utils::theme() . '.layouts.app')

@section('title', 'Forgot Password')

@section('content')

<div class="container">
  <div class="row">
  <div class="col-md-4 col-md-offset-4">
  
  <div class="panel panel-default">
	<div class="panel-heading">Forgot Password</div>

	<div class="panel-body">
	
	<form class="" action="{{route('forgot_password')}}" method="POST" data-ajax name="ForgotPassword">
    {{ csrf_field() }}
	<div class="form-group email">
	<label for="email" class="control-label">Email</label>
	<input type="email" class="form-control" name="email" placeholder="Email">
         <div class="help-block with-errors"></div>
	</div>
	 
	<div class="form-group">
	<button type="submit" class="btn btn-info btn-block" id="ForgotPassword">Send Reset Link</button>
	</div>
	
	</form>

	</div>
	
	</div>
  
  </div>
  </div>
  </div>
@endsection