@extends($utils::theme() . '.layouts.app')

@section('title', 'Update Account')

@section('content')
 <div class="container">
  <div class="row">
  
  <div class="col-md-6 col-md-offset-3">
  <h4>Basic Edit</h4>
 <form class="" action="{{route('basic_edit', ['ref'=> $user->ref])}}" method="POST" data-ajax  name="BasicEdit">
    {{ csrf_field() }}
	<div class="form-group text-center">
	<img src="{{ $user->photo_url}}" class="img-responsive img-circle img-thumbnail" alt="{{$user->username}}" width="100" id="ProfilePhoto">
	<label class="btn-bs-file btn btn-default">
       Change Photo
      <input type="file" id="UserUpload" />
     </label>
	 <textarea id="PhotoInput" name="photo_url" style="display:none;"></textarea>
	 </div>
	 
	<div class="form-group username">
	<label for="email" class="control-label">Username</label>
	<input type="text" class="form-control" name="username" placeholder="Username" value="{{$user->username}}">
         <div class="help-block with-errors"></div>
	</div> 
	<div class="form-group firstname">
	<label for="email" class="control-label">First Name</label>
	<input type="text" class="form-control" name="firstname" placeholder="First Name" value="{{$user->firstname}}">
         <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group lastname">
	<label for="email" class="control-label">Last Name</label>
	<input type="text" class="form-control" name="lastname" placeholder="Last Name" value="{{$user->lastname}}">
    <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group about">
	<label for="email" class="control-label">About</label>
	<textarea class="form-control" name="about" placeholder="About">{{ $user->about }}</textarea>
	<div class="help-block with-errors"></div>
	</div>

    <div class="form-group occupation">
	<label for="email" class="control-label">Occupation</label>
	<input type="text" class="form-control" name="occupation" placeholder="Occupation" value="{{$user->occupation}}">
    <div class="help-block with-errors"></div>
	</div>	
	
	 <div class="form-group address">
	<label for="email" class="control-label">Address</label>
	<input type="text" class="form-control" name="address" placeholder="Address" value="{{$user->address}}">
    <div class="help-block with-errors"></div>
	</div>	
	
	<div class="form-group phone">
	<label for="email" class="control-label">Phone</label>
	<input type="text" class="form-control" name="phone" placeholder="Phone" value="{{$user->phone}}">
    <div class="help-block with-errors"></div>
	</div>	
	
	<div class="form-group">
	<button type="submit" class="btn btn-info pull-righ" id="BasicEdit">Save Changes</button>
	</div>
	
	</form>
  </div>
  
  
  <div class="col-md-6 col-md-offset-3">
   <hr><br>
  <h4>Email Edit</h4>
   <form class="" action="{{route('email_edit', ['ref'=> $user->ref])}}" method="POST" data-ajax name="EmailEdit">
    {{ csrf_field() }}
	<div class="form-group email">
	<label for="email" class="control-label">Email</label>
	<input type="email" class="form-control" name="email" placeholder="Your Email" value="{{$user->email}}" >
         <div class="help-block with-errors"></div>
	</div> 
	
	<div class="form-group">
	<button type="submit" class="btn btn-info pull-right" id="EmailEdit">Save Changes</button>
	</div>
	
	</form>
  </div>
  
  
 <div class="col-md-6 col-md-offset-3">
   <hr><br>
  <h4>Password Edit</h4>
  
    <form class="" action="{{route('password_edit', ['ref'=> $user->ref])}}" method="POST" data-ajax name="PasswordEdit">
    {{ csrf_field() }}
	<div class="form-group old_password">
	<label for="old_password" class="control-label">Old Password</label>
	<input type="password" class="form-control"  name="old_password" placeholder="Old Password">
         <div class="help-block with-errors"></div>
	</div> 
	<div class="form-group password">
	<label for="password" class="control-label">Password</label>
	<input type="password" class="form-control"  name="password" placeholder="Password" id="password" required data-minlength="6">
         <div class="help-block with-errors"></div>
	</div> 
	<div class="form-group confirm_password">
	<label for="confirm_password" class="control-label">Confirm Password</label>
	<input type="password" class="form-control"  name="confirm_password" placeholder="Confirm Password" required data-minlength="6" data-match="#password">
         <div class="help-block with-errors"></div>
	</div>  
	
	<div class="form-group">
	<button type="button" class="btn btn-info pull-right" id="PasswordEdit">Save Changes</button>
	</div>
	
	</form>
      
  
  </div>
  
  
   </div>
   </div>

@endsection