<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
    <title>{{ $utils->option('site_name') }} - @yield('title')</title>
	<meta name="description" content="{{ $utils->option('site_metadescription') }}">
	<meta name="keywords" content="{{ $utils->option('site_metakeywords') }}">
	
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	
	{!! $assetic->display([
    'build/bower_components/bootstrap/dist/css/bootstrap.css',
    'build/bower_components/toastr/toastr.css',
    'build/css/app.css',
    ], false, 30) !!}
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	{!! $assetic->display([
    'build/bower_components/jquery/dist/jquery.js',
    'build/bower_components/bootstrap/dist/js/bootstrap.js',
    'build/bower_components/toastr/toastr.js',
    'build/js/ajax.js',
    'build/js/app.js',
     ], true, 30) !!}
	 
  </head>
  <body spinner="{{asset('images/loading.gif')}}">

   @include($utils::theme() . '.partials.header')
   @include($utils::theme() . '.partials.notify')
   @yield('content') 
         
  </body>
</html> 