 @extends('base.layouts.app')

@section('title', 'Sign In')

@section('content')


<div class="container">
<div class="row">
<div class="col-md-4 col-md-offset-4">

<div class="row">

    <div class="panel panel-default">
	<div class="panel-heading">Sign In</div>

	<div class="panel-body">
	
	<form class="" action="{{route('login')}}" method="POST" data-ajax id="LoginForm">
    {{ csrf_field() }}
	<div class="form-group">
	<label for="email" class="control-label">Email</label>
	<input type="email" class="form-control" name="email" placeholder="Email">
	</div>
	<div class="form-group">
	<label for="password" class="control-label">Password</label>
	<input type="password" class="form-control"  name="password" placeholder="Password">
	</div> 
	
	<div class="form-group">
	<button type="button" class="btn btn-info btn-block"  onclick="AjaxForm('LoginForm')">Sign In</button>
	</div>
	
	<div class="checkbox">
    <label>
      <input type="checkbox" name="remember"> Remember me
    </label>
        <p class="pull-right"><a href="{{route('forgot_password')}}" class="text-muted">Forgot Password?</a></p>
	</div>
	
	</form>

	</div>
	
	</div>
    
</div>
    
</div>
</div>

@endsection  