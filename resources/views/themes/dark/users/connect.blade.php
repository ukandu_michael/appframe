@extends($utils::theme() . '.layouts.app')

@section('title', 'Login Or Register')

@section('content')
   
   <div class="container">
   <div class="row">
   
   <div class="col-md-6">
   <form class="" action="{{route('login')}}" method="POST" data-ajax name="LoginForm">
    {{ csrf_field() }}
	<div class="form-group">
	<label for="email" class="control-label">Email</label>
	<input type="email" class="form-control" name="email" placeholder="Email">
	</div>
	<div class="form-group">
	<label for="password" class="control-label">Password</label>
	<input type="password" class="form-control"  name="password" placeholder="Password">
	</div> 
	
	<div class="form-group">
	<button type="button" class="btn btn-info btn-block" id="LoginForm">Sign In</button>
	</div>
	
	<div class="checkbox">
    <label>
      <input type="checkbox" name="remember"> Remember me
    </label>
        <p class="pull-right"><a href="{{route('forgot_password')}}" class="text-muted">Forgot Password?</a></p>
	</div>
	
	</form>

	<div class="col-md-12">
	<h4>OR LOGIN WITH</h4>
                    <ul class="social-network social-circle">
                        <li><a href="{{ route('social_login', ['provider'=> 'facebook']) }}" class="icoFacebook social-login-btn" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="{{ route('social_login', ['provider'=> 'twitter']) }}" class="icoTwitter social-login-btn" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="{{ route('social_login', ['provider'=> 'google']) }}" class="icoGoogle social-login-btn" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                    </ul>				
				</div>
	
   </div>
   
   <div class="col-md-6">
	
	  <form action="{{route('register')}}" role="form" method="POST" name="RegisterForm" data-ajax>
    {{ csrf_field() }} 
	<div class="form-group username">
	<label for="email" class="control-label">Username</label>
	<input type="text" class="form-control" name="username" placeholder="Username">
    <div class="help-block with-errors"></div>
	</div>
        
	<div class="form-group email">
	<label for="email" class="control-label">Email</label>
	<input type="email" class="form-control" name="email" placeholder="Email" required>
    <div class="help-block with-errors"></div>
	</div>
        
	<div class="form-group password">
	<label for="password" class="control-label">Password</label>
	<input type="password" class="form-control"  name="password" placeholder="Password" id="password" required>
    <div class="help-block with-errors"></div>
	</div> 
        
	<div class="form-group confirm_password">
	<label for="confirm_password" class="control-label">Confirm Password</label>
	<input type="password" class="form-control"  name="confirm_password" placeholder="Confirm Password" required >
    <div class="help-block with-errors"></div>
	</div> 
	
	
	<button type="button" class="btn btn-info btn-block" id="RegisterForm">Sign Up</button>
	
	
	</form>
        
    
	
   </div>
   
   </div>
   </div>
	
@endsection  