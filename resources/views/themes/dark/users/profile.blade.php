@extends($utils::theme() . '.layouts.app')

@section('title', $user->username)

@section('content')
 <div class="container">
  <div class="row">
  <div class="col-md-6 col-md-offset-3">
  <div class="panel panel-default">
  <div class="panel-body">
    <div class="text-center">
	<img src="{{ $user->photo_url}}" class="img-responsive img-circle img-thumbnail" alt="{{$user->username}}" width="100">
	</div>
	<div class="text-center">
	<h4>{{ $user->username }}</h4>
	<p><a class="btn btn-default" href="{{ route('edit_profile', ['ref'=> $user->ref]) }}" role="button">Edit Profile</a></p>
	</div>
	
  </div>
</div>
  </div>
  
   </div>
   </div>

@endsection