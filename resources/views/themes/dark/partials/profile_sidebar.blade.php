<div class="panel panel-default">
<ul class="nav nav-pills nav-stacked">
  <li class="{{Route::currentRouteName() === 'profile' ? 'active' : ''}}"><a href="{{route('profile', ['ref'=> $user->ref, 'slug'=> $user->username])}}">Profile</a></li>
  <li class="{{Route::currentRouteName() === 'basic_edit' ? 'active' : ''}}"><a href="{{route('basic_edit', ['ref'=> $user->ref])}}">Basic Edit</a></li>
  <li class="{{Route::currentRouteName() === 'password_edit' ? 'active' : ''}}"><a href="{{route('password_edit', ['ref'=> $user->ref])}}">Password Change</a></li>
  <li class="{{Route::currentRouteName() === 'email_edit' ? 'active' : ''}}"><a href="{{route('email_edit', ['ref'=> $user->ref])}}">Change Email</a></li>
  
</ul>
</div>