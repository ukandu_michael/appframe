//CREATED BY UKANDU MICHAEL
$(function(){

$('[data-ajax]').each(function(ind, val){
	
	var Form = $(this);
	var TButt = Form.attr('name');
	$('#'+TButt).on('click', function(e){
	e.preventDefault();
	$.ajax({
	url: Form.attr('action'),
	method: Form.attr('method'),
	data: Form.serializeArray(),
	beforeSend: function(){
	Loader(Form);
	},
	})
	.done(function(res){
	toastr.success(res.message, 'Success!');
	Loader(Form, false);
	window.setTimeout(function() {
    window.location.href = res.redirect_url;
		}, 1000);
	})
	
	.fail(function(err){
	var msg = $.parseJSON(err.responseText);
	//toastr.error(msg.error, 'Error!');
	Loader(Form, false);
	//console.log(msg.error);
	if (typeof msg.error === 'string' || msg.error instanceof String)
	{
		toastr.error(msg.error, 'Error!');
		//console.log(msg.error);
	}else{
	$(Form).find('.has-error').find('.help-block').text('');
	$(Form).find('.has-error').removeClass('has-error');
	
	var errors = msg.error;
	$.each(errors, function(ind, val){
		//var input = $('input[name="'+ind+'"]').find('.help-block');
		var input = $('.'+ind).find('.help-block');
		if(ind in errors){
			$('.'+ind).addClass('has-error');
			input.text(val[0]);
		}
		
	})
	}
	
	});
	
		
	});
});

var Loader = function(el, isShow = true)
{
var spinner = $('body').attr('spinner');

if(isShow){
$(el).LoadingOverlay("show", {image:spinner});
}else{
$(el).LoadingOverlay("hide");
}
}

});