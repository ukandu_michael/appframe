$(function(){
    
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
    
    $(".AppTable").DataTable();
    

    $("#createUseravatar").fileinput({
        overwriteInitial: true,
        uploadUrl:$('#createUseravatar').attr('uploadUrl'),
		uploadAsync: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        browseClass: 'btn btn-success',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open fa-lg"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="'+ $('#createUseravatar').attr('imageUrl') +'" alt="Your Avatar" style="width:160px">',
        allowedFileExtensions: ["jpg", "png"],
        initialPreviewConfig: [
            {
                 url: $('#createUseravatar').attr('deleteUrl'),
            }
        ]
    });
    
    $('#createUseravatar').on('fileuploaded', function(event, data, previewId, index) {
        var response = data.response;
        $('#file_id').val(response.file_id);
    });
    
});