$(function(){
	
	$('#UserUpload').on('change', function(){
		var file = this.files[0];
		var reader = new FileReader();
	   reader.onloadend = function(){
		 $('#ProfilePhoto').attr('src', reader.result);        
		 $('#PhotoInput').val(reader.result);        
	   }
	   reader.readAsDataURL(file);
	});
});