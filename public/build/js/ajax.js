$(function($){
    
$('[data-ajax]').each(function(ind, val){
    
    var Form = $(this);
	var btn = Form.attr('name');
    
    $('#'+btn).on('click', function(e){
        e.preventDefault();
        $.ajax({
        url: Form.attr('action'),
        method: Form.attr('method'),
        data: Form.serializeArray(),
        beforeSend: function(){
          LoadingState(Form, $('#'+btn), 1);
         $('body').trigger('BeforeFormSubmit');
        },
        })
        .done(function(res){
		toastr.success(res.message);
        LoadingState(Form, $('#'+btn), 0);
        $('body').trigger('AfterFormSubmit');
        window.setTimeout(function() {
        window.location.href = res.redirect_url;
            }, 1000);
        })
        .fail(function(err){
        var msg = $.parseJSON(err.responseText);
        LoadingState(Form, $('#'+btn), 0);
        if (typeof msg.error === 'string' || msg.error instanceof String)
        {
            toastr.error(msg.error);
        }else{
        $(Form).find('.has-error').find('.help-block').text('');
        $(Form).find('.has-error').removeClass('has-error');
        var errors = msg.error;
        $.each(errors, function(ind, val){
        var input = $('.'+ind).find('.help-block');
        if(ind in errors){
        $('.'+ind).addClass('has-error');
        input.text(val[0]);
        }

        });
        }

        });
        //error ends here
        
    });
    
    
});
    
    
var LoadingState = function(form, id, show){
    var ini_text = id.text();
    var spinner = $('body').attr('spinner');
    if(show === 1){
        $(form).find(':input').prop("disabled", true);
         id.html('<span>'+ini_text+'</span><span> <img src="'+spinner+'" width="30"></span>');
    }else{
        $(form).find(':input').prop("disabled", false);
        id.html('<span>'+ini_text+'</span>');
    }
    
}

    
});