<?php
namespace App\Components;

class Search
{
		
	public function users_search($data)
	{
		if(!empty($data)){
			
			$user = \App\User::where('username', 'like', '%' . $data['q'] . '%')
					->orWhere('ref', 'like', '%' . $data['q'] . '%')
					->orWhere('email', 'like', '%' . $data['q'] . '%')
					->orWhere('firstname', 'like', '%' . $data['q'] . '%')
					->orWhere('lastname', 'like', '%' . $data['q'] . '%')
					->whereBetween('created_at', [$data['date_from'],$data['date_to']]);
					
			return $user;
		}
	}
}