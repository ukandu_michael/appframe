<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use App\Role;

class Utils
{
	
	public function option($key){
		$option = \App\Option::where('option_key', $key)->first();
		return $option->option_value;
	}
	
	public static function mail_param($key){
		$option = \App\Option::where('option_key', $key)->first();
		return $option->option_value;
	}
	
	public static function theme(){
		$option = \App\Option::where('option_key', 'theme')->first();
		return $option->option_value;
	}
	
	public static function setMailable(){
		
		$config = config([
		'mail.driver'=> Utils::mail_param('mail_driver'),
		'mail.host'=> Utils::mail_param('mail_server'),
		'mail.port'=> Utils::mail_param('mail_port'),
		'mail.encryption'=> Utils::mail_param('mail_encryption'),
		'mail.username'=> Utils::mail_param('mail_login'),
		'mail.password'=> Utils::mail_param('mail_password'),
		]);
		
		return $config;
	}
	
}
