<?php
namespace App\Helpers;

use \MatthiasMullie\Minify;
use \App\Helpers\Utils;

class Assetic
{
	public $compress;
	
	public $files;
	
	public $is_js;
	
	public $util;
	
	public function __construct()
	{
		$this->util = new Utils();
	}
	
	public function display($files = [], $is_js = false, $id)
	{
		$status = $this->util->option('mode');
		if($status === 'DOWN'){
			return $this->ListForDown($files, $is_js);
		}elseif($status === 'LIVE'){
			$path_sw = !$is_js ? '/css' : '/js';
			//$path = public_path() . $path_sw;
			$path = public_path() . '/src';
			$dircheck = !$is_js ? glob($path . "/*$id.css") : glob($path . "/*$id.js") ;
			//print_r(count($dircheck));
			if(count($dircheck) < 1){
				return $this->ListForBuild($files, $is_js, $id);
			}else{
				$pas_path = [];
				foreach($dircheck as $filen){
				if($is_js){ 
				$path_path[] = '<script src="' . asset('src/' . basename($filen)) . '"></script>';
				}else{
					$path_path[] = '<link rel="stylesheet" href="' . asset('src/' . basename($filen)) . '">';
				}
					
				}
				return implode("\n", $path_path);
			}
		}
	}
	
	public function ListForDown($files, $is_js = false){
		
		//$public_path = public_path();
		$list = [];
		if(!empty($files)){
			foreach($files as $file){
				if($is_js){ 
				$list[] = '<script src="' . asset($file) . '"></script>';
				}else{
					$list[] = '<link rel="stylesheet" href="' . asset($file) . '">';
				}
			}
		}
		return implode("\n", $list);
	}
	
	public function ListForBuild($files, $is_js = false, $id){
		$container = [];
		if(!empty($files)){
			foreach($files as $file){
				if(file_exists(public_path($file))){
				$container[] = file_get_contents(public_path($file));
				}
			}
		}
		$script =  implode("\n", $container);
		
		if(!$is_js){
		$minifier = new Minify\CSS($script);
		$filename = 'bundle.' . \vakata\random\Generator::string(5, config('site.generator')) . $id . '.css';
		$path = public_path() . '/src/' . $filename;
		file_put_contents($path, $minifier->minify());
		return '<link rel="stylesheet" href="' . asset('src/' . $filename) . '?ver="'. $this->util->option('version') .'>';
		
		}else{
		$minifier = new Minify\JS($script);
		$filename = 'bundle.' . \vakata\random\Generator::string(5, config('site.generator')) . $id . '.js';
		$path = public_path() . '/src/' . $filename;
		file_put_contents($path, $minifier->minify());
		return '<script src="' . asset('src/' . $filename) . '?ver="'. $this->util->option('version') .'></script>';
		
		}
	}
	
}