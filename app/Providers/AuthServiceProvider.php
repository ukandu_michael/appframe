<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    
    public $permissions = [
       'CREATE',
       'READ',
       'UPDATE',
       'DELETE'
    ];
    
    
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        
        
        Gate::define('Registered', function ($user, $model = null) {
            
            $roles = json_decode($user->role->permissions, true);
            
            if(in_array('READ', $roles))
            {
                return true;
                
            }elseif($model AND $user->id === $model->user_id)
            {
                return true;
                
            }else{
                
                return false;
            }
            
        });
        
    
    Gate::define('Author', function ($user, $model = null) {
            
            $roles = json_decode($user->role->permissions, true);
            
            if(in_array(['CREATE', 'READ'], $roles))
            {
                return true;
                
            }elseif($model AND $user->id === $model->user_id)
            {
                return true;
                
            }else{
                
                return false;
            }
            
    });
        
    
    Gate::define('SystemAdmin', function ($user, $model = null) {
            
            $roles = json_decode($user->role->permissions, true);
            
            if(in_array(['CREATE', 'READ', 'UPDATE'], $roles) AND $user->is_admin)
            {
                return true;
                
            }elseif($model AND $user->id === $model->user_id)
            {
                return true;
                
            }else{
                
                return false;
            }
            
    });
        
    
    Gate::define('GlobalAdmin', function ($user, $model = null) {
            
            $roles = json_decode($user->role->permissions, true);
            
            if(in_array(['CREATE', 'READ', 'UPDATE', 'DELETE'], $roles) AND $user->is_admin)
            {
                return true;
                
            }elseif($model AND $user->id === $model->user_id)
            {
                return true;
                
            }else{
                
                return false;
            }
            
    });
        
   
        
    }
}
