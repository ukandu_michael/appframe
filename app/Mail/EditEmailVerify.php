<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EditEmailVerify extends Mailable
{
    use Queueable, SerializesModels;

    public $user; 
	
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		return $this->from(config('site.site_email'), config('site.site_name') . ' ' . config('site.site_description'))
                ->subject(config('site.site_name') . ' - New Email Verification')
				->view('emails.edit_email_verify');
    }
}
