<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Token;
use App\Mail\EmailVerify;
use App\Mail\ForgotPassword;
use App\User;
use App\Role; 
use \App\Helpers\Utils;
use Socialite;

class AuthCtrl extends Controller
{ 
    
   public function connect(Request $request)
	{	
		return view(Utils::theme() . '.users.connect');
	} 
	
	public function login(Request $request)
    {
        if($request->isMethod('post')){
			$email = $request->email;
			$password = $request->password;
			$remember = !$request->remember ? false : true;
			
			if (Auth::attempt(['email' => $email, 'password' => $password, 'is_active' => true], $remember)) {
				return response()->json(['message'=> 'You have successfully logged In.', 'redirect_url'=> route('home')], 200);
			}else{
				return response()->json(['error'=> 'Invalid Email or Password.'], 500);
			}
		}
       
    }
	
	public function register(Request $request)
    {
		
        if($request->isMethod('post')){
			
			$validator = Validator::make($request->all(), [
				'username' => 'required|max:255|min:4|alpha_num|unique:users',
				'password' => 'required|max:255|min:6',
				'confirm_password' => 'required|max:255|min:6|same:password',
				'email' => 	  'required|email|unique:users',
			]);
			
			if ($validator->fails()) {
				return response()->json(['error'=> $validator->errors()], 500);
				
			}else{
                $role = Role::where('is_default', true)->first();
				$user = new User();
				$user->username = $request->username;
				$user->slug = str_slug($request->username);
				$user->ref = Token::getRandomString(10);
				$user->role_id = $role->id;
				$user->email = $request->email;
				//$user->photo_url = asset('images/avatar.png');
				$user->password = bcrypt($request->password);
				$user->email_token = Token::getRandomString(36);
				$user->email_token_expires = \Carbon\Carbon::now()->addDay();
				if($user->save()){
					Utils::setMailable();
					 $when = \Carbon\Carbon::now()->addSeconds(2);
					 Mail::to($user->email)->later($when, new EmailVerify($user));
					return response()->json(['message'=> 'Your registration was successfull', 'redirect_url'=> route('home')], 200);
				}else{
					return response()->json(['error'=> 'There was an error saving user'], 500);
				}
			}
			
		}
        
    }
	
	public function verify(Request $request, $token)
    {
        if($request->isMethod('get')){
			$user = \App\User::where('email_token', $token)->first();
			if(!$user){
				return redirect()->route('home')->with('error', 'Invalid Verification Token.');
			}else{
				$user->email_token = null;
				$user->email_token_expires = null;
				$user->email_confirmed = true;
				$user->is_active = true;
				if($user->save()){
					return redirect()->route('home')->with('success', 'Your Account verification was successfull, You may now login.');
				}
			}
		}
    }
	
	public function forgot_password(Request $request)
    {
        
        if($request->isMethod('post')){
			$validator = Validator::make($request->all(), [
				'email' => 	  'required|email|exists:users',
			],[
			'email.exists'=> 'This email address is invalid.'
			]);
			if ($validator->fails()) {
				return response()->json(['error'=> $validator->errors()->first()], 500);
				
			}else{
				$user = \App\User::where('email', $request->email)->first();
				
				$user->password_token = Token::getRandomString(36);
				$user->password_token_expires = \Carbon\Carbon::now()->addDay();
				if($user->save())
				{
					Utils::setMailable();
					$when = \Carbon\Carbon::now()->addSeconds(2);
					 Mail::to($user->email)->later($when, new ForgotPassword($user));
					return response()->json(['message'=> 'A Password reset link has been sent to your email.', 'redirect_url'=> route('connect')], 200);
				}
			}
        }
			
        return view(Utils::theme() . '.users.forgot_password');
    }
	
	 public function reset_password(Request $request, $token)
    {
        
        if($request->isMethod('get'))
		{
			$user = \App\User::where('password_token', $token)->first();
			if(!$user)
			{
				return redirect()->route('home')->with('error', 'Invalid Token, please check your link and try again.');
			}
		}
		
		if($request->isMethod('post')){
			$validator = Validator::make($request->all(), [
				'password' => 'required|max:255|min:6',
				'confirm_password' => 'required|max:255|min:6|same:password',
			]);
			
			if ($validator->fails()) {
				return response()->json(['error'=> $validator->errors()->first()], 500);
				
			}else{
			 $user = \App\User::where('password_token', $token)->first();
			 $user->password = bcrypt($request->password);
			 $user->password_token = null;
			 $user->password_token_expires = null;
			 if($user->save())
			 {
				return response()->json(['message'=> 'Your password reset was successfull, you may now login.', 'redirect_url'=> route('connect')], 200); 
			 }
			 
			}
		}
        
        return view(Utils::theme() . '.users.reset_password', ['token'=> $token]);
    }
    
	public function redirectToProvider(Request $request, $provider)
    {
        return Socialite::driver($provider)->redirect();
    }
	
	public function social_login(Request $request, $provider)
    {
        session(['redirectUrl' => $request->input('redirect')]);
        return Socialite::driver($provider)->redirect();
    }
	
	public function social_callback(Request $request, $provider)
    {
        $redUrl = session('redirectUrl');
        $data = Socialite::driver($provider)->user();
		$userCheck = \App\User::where($provider, $data->getId())->orWhere('email', $data->getEmail())->first();
		if($userCheck){
            if(!$userCheck->$provider){
                $userCheck->$provider = $data->getId();
               if($userCheck->save()){
                   Auth::loginUsingId($userCheck->id, true);
                 }
                }else{
                Auth::loginUsingId($userCheck->id, true);
                 }
             return redirect($redUrl)->with('success', 'You have successfully logged in with ' . $provider);
             
			}else{
		$role = Role::where('is_default', true)->first();
		$user = new User();
		$user->username = $data->getName();
		$user->slug = str_slug($data->getName());
		$user->ref = \vakata\random\Generator::string(10, config('site.generator'));
		$user->role_id = $role->id;
		$user->email = $data->getEmail();
		$user->photo_url = $data->getAvatar();
		$user->password = 'Not Available';
		$user->$provider = $data->getId();
		if($user->save()){
			Auth::loginUsingId($user->id, true);
			return redirect($redUrl)->with('success', 'You have successfully logged in with ' . $provider);
		}
	  }
    }
	
    
}
