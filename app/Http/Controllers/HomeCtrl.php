<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Helpers\Utils;

class HomeCtrl extends Controller
{
    
	public function index(Request $request)
	{
		return view(Utils::theme() . '.home');
	}
}
 