<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Mail\EditEmailVerify;
use App\Helpers\Token;
use \App\Helpers\Utils;

class UsersCtrl extends Controller
{
    
    public function logout(Request $request)
    {
        if($request->isMethod('post')){
			Auth::logout();
		    return redirect()->route('home')->with('success', 'You have logged out successfully.');
		}
    }
    
    public function profile(Request $request, $ref)
    {
        if($request->isMethod('get')){
			
			$user = \App\User::where([['ref', $ref]])->first();
		}
        return view(Utils::theme() . '.users.profile', ['user'=> $user]);
    }
    
    public function edit(Request $request, $ref)
    {
        if($request->isMethod('get')){
			
			$user = \App\User::where([['ref', $ref]])->first();
		}
        return view(Utils::theme() . '.users.edit', ['user'=> $user]);
    }
    
    
    public function basic_edit(Request $request, $ref)
    {
        
        /*if($request->isMethod('get')){
			
			$user = \App\User::where('ref', $ref)->first();
            if(!$user)
			{
				return redirect()->route('home')->with('error', 'Invalid User, please check your link and try again.');
			}
		}*/
		
		if($request->isMethod('post')){
			
			$user = \App\User::where('ref', $ref)->first();
			$validator = Validator::make($request->all(), [
				'username' => 'required|max:255|min:4|alpha_num|unique:users,email,'.$user->id,
				'firstname' => 'max:255|min:4',
				'lastname' => 'max:255|min:4',
				'about' => 'max:255|min:4',
				'occupation' => 'max:255|min:4',
				'address' => 'max:255|min:4',
				'phone' => 'digits:11',
			]);
			
			if ($validator->fails()) {
				return response()->json(['error'=> $validator->errors()], 500);
				
			}else{
				if(!is_null($user->photo_url)){
				$old_photo = basename($user->photo_url);
				if(file_exists(public_path('uploads/users/'). $old_photo)){
				unlink(public_path('uploads/users/'). $old_photo);
				}
				}
				
				
				$user->username = $request->username;
				$user->firstname = $request->firstname;
				$user->lastname = $request->lastname;
				$user->about = $request->about;
				$user->occupation = $request->occupation;
				$user->address = $request->address;
				$user->phone = $request->phone;
				$user->photo_url = is_null($request->photo_url) ? $user->photo_url : $this->base64_img($request->photo_url, str_slug($request->username), 'uploads/users');
				if($user->save()){
					return response()->json(['message'=> 'Your Profile updated was successfull.', 'redirect_url'=> route('edit_profile', ['ref'=>$user->ref])], 200);
				}
			}
			
		}
        
    }
    
    
    
    public function email_edit(Request $request, $ref)
    {
        
        if($request->isMethod('get'))
		{
			$user = \App\User::where('ref', $ref)->first();
			if(!$user)
			{
				return redirect()->route('home')->with('error', 'Error Fetching user, please check your link and try again.');
			}
		}
		
		if($request->isMethod('post')){
			$user = \App\User::where('ref', $ref)->first();
			
			$validator = Validator::make($request->all(), [
				'email' => 	  'required|email|unique:users,email,'.$user->id,
			]);
			
			if ($validator->fails()) {
				return response()->json(['error'=> $validator->errors()], 500);
				
			}elseif($user->email === $request->email){
				return response()->json(['error'=> 'You have not made any changes.'], 500);
				
			}else{
			 
			 $user->email = $request->email;
			 $user->is_active = null;
			 $user->email_confirmed = null;
			 $user->email_token = Token::getRandomString(36);
			 $user->email_token_expires = \Carbon\Carbon::now()->addDay();
			 if($user->save())
			 {
				Auth::logout();
				Utils::setMailable();
				$when = \Carbon\Carbon::now()->addSeconds(2);
				Mail::to($user->email)->later($when, new EditEmailVerify($user));
				return response()->json(['message'=> 'Your email has been updated successfully and an email verification link has been sent to your new email. Please verify your new email to be able to login again.', 
				'redirect_url'=> route('home')], 200); 
			 }
			 
			}
		}
        
    }
    
    
    
    public function email_verify(Request $request, $token)
    {
         if($request->isMethod('get')){
			$user = \App\User::where('email_token', $token)->first();
			if(!$user){
				return redirect()->route('home')->with('error', 'Invalid Verification Token.');
			}else{
             $user->is_active = true;
             $user->email_confirmed = true;
             $user->email_token = null;
             $user->email_token_expires = null;
				if($user->save()){
					return redirect()->route('connect')->with('success', 'Your New Email Has been Verified, You may now login.');
				}
			}
		}
    }
    
    
    public function password_edit(Request $request, $ref)
    {
        if($request->isMethod('get')){
			
			$user = \App\User::where('ref', $ref)->first();
            if(!$user)
			{
				return redirect()->route('home')->with('error', 'Invalid User, please check your link and try again.');
			}
		}
        
        if($request->isMethod('post')){
			$user = \App\User::where('ref', $ref)->first();
			
			$validator = Validator::make($request->all(), [
				'old_password' => 'required|max:255|min:6',
				'password' => 'required|max:255|min:6',
				'confirm_password' => 'required|max:255|min:6|same:password',
			]);
			
			if ($validator->fails()) {
				return response()->json(['error'=> $validator->errors()], 500);
				
			}elseif(!Hash::check($request->old_password, $user->password)){
				
				return response()->json(['error'=> 'Your Old Password is Invalid.'], 500);
				
			}else{
			 
			 $user->password = bcrypt($request->password);
			 if($user->save())
			 {
				return response()->json(['message'=> 'Your password was successfully changed.', 'redirect_url'=> route('edit_profile', ['ref'=>$user->ref])], 200); 
			 }
			 
			}
		}
        
    }
    
	 public function base64_img($data, $name, $folder){
		//$data = str_replace('data:image/png;base64,', '', $data);
		$data = substr($data, strpos($data, ',') + 1);
		$data = str_replace(' ', '+', $data);
		$dat = base64_decode($data);
		$filename = $name . '-' . Token::getRandomString(10) . '.png';
		$url = public_path() . '/'. $folder .'/' . $filename;
		file_put_contents($url, $dat);
		return asset($folder .'/' . $filename); 
	 }
	 
    
    
    
}
