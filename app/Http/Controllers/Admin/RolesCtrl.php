<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Role;
use App\User;

class RolesCtrl extends Controller
{
    
   public $permissions = [
       'CREATE',
       'READ',
       'UPDATE',
       'DELETE'
   ];
    
    
   public function index(Request $request)
   {
      $roles = Role::paginate(20);
       
       return view('admin.roles.index', ['roles'=> $roles]);
   }  
    
   public function add(Request $request)
   {
       
       $default_role = Role::where('is_default', true)->first();
       
       if($request->isMethod('post'))
       {
           if($default_role && !is_null($request->is_default))
            {
                return response()->json(['error'=> 'default role exists already.' ], 500);
                
            }else{
                
                $validator = Validator::make($request->all(), [
				'name' => 'required|max:255|min:4|unique:roles',
                'permissions'=> 'required',
                'is_default'=> 'boolean'
                ]);

                if ($validator->fails()) {
                    return response()->json(['error'=> $validator->errors()], 500);

                }else{

                    $role = new Role();

                    $role->name = $request->name;
                    $role->is_default = is_null($request->is_default) ? false : true;
                    $role->permissions = json_encode($request->permissions, true);

                    if($role->save())
                    {
                        return response()->json(['message'=> 'role was added successfully', 'redirect_url'=> route('admin_role')], 200);
                    }
                }
            }
       }
       
       return view('admin.roles.add', ['permissions'=> $this->permissions]);
   }
    
    
   public function edit(Request $request, $id)
   {
       $role = Role::where('id', $id)->firstorFail();
       $default_role = Role::where('is_default', true)->first();
       
       //RESOLVE PERMISSIONS AND SEND IT TO THE VIEW
       $perm = [];
       
       $array = json_decode($role->permissions, true);
       foreach($this->permissions as $value)
       {
           $perm[] = ['name'=> $value, 'status'=> in_array($value, $array) ? true : false, ];
       }
       
       $permissions = json_decode(json_encode($perm));
    
       //VALIDATE AND SAVE FORM
       
       if($request->isMethod('post'))
		{
			if($request->is_default AND $default_role->id !== $role->id)
			{
				return response()->json(['error'=> 'default role exists already.' ], 500);
			}else{
				
				$validator = Validator::make($request->all(), [
				'name' => 'required|max:255|min:4|alpha_dash|unique:roles,name,' . $role->id,
				'permissions'=> 'required',
                'is_default'=> 'boolean'
			]);
			
			if ($validator->fails()) {
				return response()->json(['error'=> $validator->errors()], 500);
				
				}else{
					
					$role->name = is_null($request->name) ? $role->name : $request->name;
					$role->is_default = is_null($request->is_default) ? false : true;
                    $role->permissions = json_encode($request->permissions, true);
                
					if($role->save())
					{		
						return response()->json(['message'=> 'role was updated successfully', 'redirect_url'=> route('admin_role') ], 200);
					}
				}
			
			}
			
		}
       
      
       return view('admin.roles.edit', ['role'=> $role, 'permissions'=> $permissions]);
   }
    
    
   public function delete(Request $request, $id)
   {
      $role = Role::where('id', $id)->firstOrFail();
       
       if($role->is_default || $role->count() <= 1)
       {
           return redirect()->route('admin_role')->with('error', 'You cannot delete default role.');
           
       }else{
           
           $default_role = Role::where('is_default', true)->first();
           DB::table('users')->where('role_id', $role->id)->update(['role_id' => $default_role->id]);
           if($role->delete())
           {
                return redirect()->route('admin_role')->with('success', 'Role has been delete successfully.');
           }
       }
   }
    
    
   public function default_toggle(Request $request, $id, $action)
   {
      $role = Role::where('id', $id)->firstOrFail();
      $default_role = Role::where('is_default', true)->first();
       
       if($default_role AND $default_role->id !== $role->id)
       {
           return redirect()->route('admin_role')->with('error', 'default role exists already.');
           
       }else{
           
		if($action AND $action === 'remove')
		{
			$role->is_default = false;
            
            if($role->save())
            {
                return redirect()->route('admin_role')->with('success', 'Role has been removed as default');
            }
		}elseif($action AND $action === 'add')
		{
			$role->is_default = true;
            if($role->save())
            {
                return redirect()->route('admin_role')->with('success', 'Role has been made default');
            }
		}
       }
		
   }
    
    
    
    
}
