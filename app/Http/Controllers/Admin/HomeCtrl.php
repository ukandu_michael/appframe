<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeCtrl extends Controller
{
    
    public function __construct()
	{
	    $this->middleware('admin');
	}
    
    
    public function dashboard(Request $request)
    {
        
        return view('admin.dashboard');
    }
}
