<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;


class OptionsCtrl extends Controller
{
    public function index(Request $request)
   {
	  $themes = [];
	  $dir = resource_path('views/themes/');
	  $directories = glob($dir . '*' , GLOB_ONLYDIR);
	  foreach($directories as $path){
		  $res = 'themes.' . basename($path);

		  $themes[] = ['dir'=> $res, 'name'=> title_case(basename($path))];
	  }
	   
       return view('admin.options.index', ['themes'=> $themes]);
   }
   
   public function update_basic(Request $request)
   {
		if($request->isMethod('post'))
        {
			$data = $request->all();
			
			foreach($data as $key => $val){
				DB::table('options')->where('option_key', $key)->update(['option_value'=> $val]);
			}
			return response()->json(['message'=> 'Changes was saved successfully.', 'redirect_url'=> route('admin_options')], 200);
            	
		}
   }
   
   public function update_mail(Request $request)
   {
		if($request->isMethod('post'))
        {
			$data = $request->all();
			
			foreach($data as $key => $val){
				DB::table('options')->where('option_key', $key)->update(['option_value'=> $val]);
			}
			return response()->json(['message'=> 'Changes was saved successfully.', 'redirect_url'=> route('admin_options')], 200);
            	
		}
   }
   
   public function update_theme(Request $request)
   {
		if($request->isMethod('post'))
        {
			$data = $request->all();
			
			foreach($data as $key => $val){
				DB::table('options')->where('option_key', $key)->update(['option_value'=> $val]);
			}
			return response()->json(['message'=> 'Changes was saved successfully.', 'redirect_url'=> route('admin_options')], 200);
            	
		}
   }
   
   

}
