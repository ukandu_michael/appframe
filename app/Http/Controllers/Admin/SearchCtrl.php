<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchCtrl extends Controller
{
    public function search(Request $request, $route_name)
    {
        if($request->isMethod('post')){
			
			$data = $request->all();
			
			return redirect()->route($route_name, $data['search_term']);
		}
    }
	
}
