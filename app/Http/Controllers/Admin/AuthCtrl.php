<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthCtrl extends Controller
{
    
   public function login(Request $request)
   {
       if($request->isMethod('post'))
        {
            $email = $request->email;
			$password = $request->password;
			$remember = !$request->remember ? false : true;
            
           if (Auth::attempt(['email' => $email, 'password' => $password, 'is_active' => true, 'is_admin'=>true], $remember)) {
               
                   return response()->json(['message'=> 'You have successfully logged In.', 'redirect_url'=> route('admin')], 200);
            
			}else{
				return response()->json(['error'=> 'Invalid email or password.', 'redirect_url'=> route('admin_login')], 500);
            	
			}
        }
        
       return view('admin.users.login');
   } 
    
    
}
