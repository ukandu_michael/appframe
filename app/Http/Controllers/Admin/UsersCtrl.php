<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Utils;
use App\Helpers\Token;
use App\User;
use App\Role; 

class UsersCtrl extends Controller
{
    
   public function __construct()
	{
	    $this->middleware('admin');
	}
    
    
   public function logout(Request $request)
   {
       if($request->isMethod('post'))
        {
            
                Auth::logout();
               
               return redirect()->route('admin_login')->with('success', 'You have successfully logged Out.');
        }
   } 
    

   public function index(Request $request)
   {
	   if(!empty($request->input()))
	   {
		$search = new \App\Components\Search();
		$users = $search->users_search($request->input())->paginate(20);
	   }else{
       $users = User::paginate(20);
	   }
       return view('admin.users.index', ['users'=> $users]);
   }

  
   public function add(Request $request)
   {
       $roles = Role::all();
      
       if($request->isMethod('post'))
		{
			$validator = Validator::make($request->all(), [
				'username' => 'required|max:255|min:6|alpha_num|unique:users',
				'password' => 'required|max:255|min:6',
				'confirm_password' => 'required|max:255|min:6|same:password',
				'email' => 	  'required|email|unique:users',
				'role' => 	  'required',
				'firstname' => 	  'min:6',
				'lastname' => 	  'min:6',
				'is_admin' => 	  'boolean',
			]);
			
			if ($validator->fails()) {
				return response()->json(['error'=> $validator->errors()], 500);
				
			}else{
    
				$user = new User();
				
				$user->ref =  \vakata\random\Generator::string(10, config('site.generator'));
				$user->is_active =  true;
				$user->is_admin =  is_null($request->is_admin) ? false : true;
				$user->email_confirmed =  true;
				$user->role_id =  $request->role;
				$user->username =  $request->username;
				$user->slug =  str_slug($request->username);
				$user->password =  bcrypt($request->password);
				$user->email    =    $request->email;
				$user->firstname    =   is_null($request->firstname) ? '' : $request->firstname;
				$user->lastname    =   is_null($request->lastname) ? '' : $request->lastname;
                $user->photo_url = $request->photo_url;
				
				if($user->save())
				{
					return response()->json(['message'=> 'user was created successfully', 'redirect_url'=> route('admin_user')], 200);
				}
			}
		}
       
       return view('admin.users.add', ['roles'=> $roles]);
   }
    
    
    
   public function edit(Request $request, $ref)
   {
       $user = User::where('ref', $ref)->firstOrFail();
       
       if($request->isMethod('post'))
		{
			$validator = Validator::make($request->all(), [
				'username' => 'required|max:255|min:6|alpha_num|unique:users,username,' . $user->id,
				'email' => 	  'required|email|unique:users,email,' . $user->id,
				'role' => 	  'required',
				'firstname' => 	  'min:6',
				'lastname' => 	  'min:6',
                'is_admin' => 	  'boolean',
			]);
			
			if ($validator->fails()) {
				return response()->json(['error'=> $validator->errors()], 500);
				
			}else{
    
                $user->is_admin =  is_null($request->is_admin) ? false : true;
				$user->email_confirmed =  true;
				$user->role_id =  $request->role;
				$user->username =  $request->username;
				$user->email    =    $request->email;
				$user->firstname    =   is_null($request->firstname) ? '' : $request->firstname;
				$user->lastname    =   is_null($request->lastname) ? '' : $request->lastname;
                $user->photo_url = $request->photo_url;
				
				if($user->save())
				{
					return response()->json(['message'=> 'user was updated successfully', 'redirect_url'=> route('admin_user')], 200);
				}
			}
		}
       
       return view('admin.users.edit', ['user'=> $user]);
   }
    
    
   public function edit_password(Request $request, $ref)
   {
       
       $user = User::where('ref', $ref)->first();
       
       if($request->isMethod('post')){
			
			$validator = Validator::make($request->all(), [
				'password' => 'required|max:255|min:6',
				'confirm_password' => 'required|max:255|min:6|same:password',
			]);
			
			if ($validator->fails()) {
				return response()->json(['error'=> $validator->errors()], 500);
				
			}elseif(!Hash::check($request->old_password, $user->password)){
				
				return response()->json(['error'=> 'Your Old Password is Invalid.'], 500);
				
			}else{
			 
			 $user->password = bcrypt($request->password);
                
			 if($user->save())
			 {
				return response()->json(['message'=> 'Your password was successfully updated', 'redirect_url'=> route('admin_user')], 200); 
			 }
			 
			}
		}
       
       return view('admin.users.edit_password', ['user'=> $user]);
   }
    
    
    
   public function delete(Request $request, $ref)
   {
      $user = User::where('ref', $ref)->firstOrFail();
       
         if($user->delete())
		 {
            Utils::convertAndRemoveFile($user->file_ids);
			return redirect()->route('admin_user')->with('success', 'You was successfully deleted.');
		 }
   }
    

   public function activate_toggle(Request $request, $ref, $action)
	{
		$user = User::where('ref', $ref)->firstOrFail();
		
		if($action AND $action === 'deactivate')
		{
			$user->is_active = false;
            if($user->save())
            {
                return redirect()->route('admin_user')->with('success', 'User is now inactive');
            }
		}elseif($action AND $action === 'activate')
		{
			$user->is_active = true;
            if($user->save())
            {
                return redirect()->route('admin_user')->with('success', 'User is now active');
            }
		}
		
	}
    
    
    
    
}
