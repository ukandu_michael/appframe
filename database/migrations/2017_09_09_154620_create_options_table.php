<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('options', function (Blueprint $table) {
            $table->increments('id');
            $table->string('option_key');
            $table->longText('option_value');
            $table->timestamps();
        });
		
		if (DB::table('options')->count() < 1 )
        {
           DB::table('options')->insert([
                [
                    'option_key' => 'site_url',
                    'option_value' => url('/'),
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
				[
                    'option_key' => 'site_name',
                    'option_value' => 'App Frame',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
				[
                    'option_key' => 'site_description',
                    'option_value' => 'App Frame is a web application starter built by ukandu michael ikemuefuna.',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
				[
                    'option_key' => 'site_metakeywords',
                    'option_value' => 'app starter, laravel, ukandu michael',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
				[
                    'option_key' => 'site_metadescription',
                    'option_value' => 'App Frame - Build apps easier and faster.',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
				[
                    'option_key' => 'theme',
                    'option_value' => 'themes.default',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
				[
                    'option_key' => 'mail_server',
                    'option_value' => 'smtp.gmail.com',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
				[
                    'option_key' => 'mail_login',
                    'option_value' => 'vcordukandu@gmail.com',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
				[
                    'option_key' => 'mail_password',
                    'option_value' => 'kingukandu1989',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
				[
                    'option_key' => 'mail_port',
                    'option_value' => '587',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
				[
                    'option_key' => 'mail_encryption',
                    'option_value' => 'tls',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
				[
                    'option_key' => 'mail_driver',
                    'option_value' => 'smtp',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
				[
                    'option_key' => 'version',
                    'option_value' => 'v1.1.3',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
				[
                    'option_key' => 'mode',
                    'option_value' => 'DOWN',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ]
				
            ]);
        }
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('options');
    }
}
