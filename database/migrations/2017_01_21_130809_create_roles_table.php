<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('permissions'); //json [create, read, update, delete]
            $table->boolean('is_default');
			$table->timestamps();
        });
        
        
        if (DB::table('roles')->count() < 1 )
        {
           DB::table('roles')->insert([
                [
                    'name' => 'admin',
                    'permissions' => '["CREATE","READ","UPDATE","DELETE"]' ,
                    'is_default' => false,
                ],
               
               [
                    'name' => 'registered',
                    'permissions' => '["READ"]' ,
                    'is_default' => true,
                ]
               
            ]);
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
