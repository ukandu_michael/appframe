<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id');
            $table->string('username');
            $table->string('slug');
            $table->string('ref');
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->boolean('is_active')->nullable();
            $table->boolean('is_admin')->nullable();
            $table->boolean('email_confirmed')->nullable();
            $table->string('email_token')->nullable();
            $table->string('password_token')->nullable();
			$table->timestamp('email_token_expires')->nullable();
			$table->timestamp('password_token_expires')->nullable();
			$table->string('firstname')->nullable();
			$table->string('lastname')->nullable();
			$table->longText('about')->nullable();
			$table->longText('photo_url')->nullable();
			$table->string('occupation')->nullable();
			$table->string('address')->nullable();
			$table->string('phone')->nullable();
			$table->string('facebook')->nullable();
			$table->string('twitter')->nullable();
			$table->string('google')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
        
        if (DB::table('users')->count() < 1 )
        {
           DB::table('users')->insert(
                [
                    'role_id' => 1,
                    'username' => 'admin',
                    'slug' => str_slug('admin', '_'),
                    'ref' => str_random(10),
                    'email' => 'admin@gmail.com',
                    'password' => bcrypt('admin1234'),
                    'is_active' => true,
                    'is_admin' => true,
                    'email_confirmed' => true,
                    'firstname' => 'admin',
                    'lastname' => 'admin',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
