<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
    As the name suggests, this is where the front end routes are registered.
    neccessary for a well structured application.
*/

require_once base_path('routes/frontend.php');

/**
    Include Admin Routes
    This was necessary in order to seperate logic and provide a well structured application.
*/

require_once base_path('routes/admin.php');