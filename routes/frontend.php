<?php

/*
    As the name suggests, this is where the front end routes are registered.
    neccessary for a well structured application.
*/

 

Route::group(['prefix' => ''], function () {
    Route::match(['post', 'get'], '/', 'HomeCtrl@index')->name('home');
    
    //User Authentication Routes
    Route::match(['post', 'get'], '/connect', 'AuthCtrl@connect')->name('connect');
	Route::match(['post', 'get'], '/login', 'AuthCtrl@login')->name('login');  
	Route::match(['post', 'get'], '/register', 'AuthCtrl@register')->name('register');
	Route::match(['post', 'get'], '/verify/{token}', 'AuthCtrl@verify')->name('verify'); 
    Route::match(['post', 'get'], '/forgot-password', 'AuthCtrl@forgot_password')->name('forgot_password'); 
    Route::match(['post', 'get'], '/reset-password/{token}', 'AuthCtrl@reset_password')->name('reset_password'); 
	Route::match(['post', 'get'], '/email-verify/{token}', 'UsersCtrl@email_verify')->name('edit_email_verify'); 
	Route::match(['post', 'get'], '/auth/{provider}', 'AuthCtrl@social_login')->name('social_login');
    Route::match(['post', 'get'], '/auth/callback/{provider}', 'AuthCtrl@social_callback')->name('social_callback'); 

 });

//User Management Routes
Route::group(['prefix' => '/user'], function () {
    Route::match(['post', 'get'], '/logout', 'UsersCtrl@logout')->name('logout'); 
    Route::match(['post', 'get'], '/profile/{ref}', 'UsersCtrl@profile')->name('profile'); 
    Route::match(['post', 'get'], '/edit/{ref}', 'UsersCtrl@edit')->name('edit_profile'); 
    Route::match(['post', 'get'], '/basic-edit/{ref}', 'UsersCtrl@basic_edit')->name('basic_edit'); 
    Route::match(['post', 'get'], '/email-edit/{ref}', 'UsersCtrl@email_edit')->name('email_edit'); 
    Route::match(['post', 'get'], '/password-edit/{ref}', 'UsersCtrl@password_edit')->name('password_edit'); 
});