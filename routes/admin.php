<?php

/**
    THE ADMIN ROUTES
*/

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
//DASHBOARD ROUTE
Route::match(['get', 'post'], '/dashboard', 'HomeCtrl@dashboard')->name('admin');
Route::match(['get', 'post'], '/login', 'AuthCtrl@login')->name('admin_login');
Route::match(['get', 'post'], '/seach/{route_name}', 'SearchCtrl@search')->name('admin_search');

//USERS ROUTES
Route::match(['get', 'post'], '/user/all', 'UsersCtrl@index')->name('admin_user');
Route::match(['get', 'post'], '/user/add', 'UsersCtrl@add')->name('admin_add_user');
Route::match(['get', 'post'], '/user/edit/{ref}', 'UsersCtrl@edit')->name('admin_edit_user');
Route::match(['get', 'post'], '/user/edit/password/{ref}', 'UsersCtrl@edit_password')->name('admin_edit_user_password');
Route::match(['get', 'post'], '/user/activate-deactivate/{ref}/{action}', 'UsersCtrl@activate_toggle')->name('admin_user_activate_toggle');
Route::match(['get', 'post'], '/user/delete/{ref}', 'UsersCtrl@delete')->name('admin_delete_user');
Route::match(['get', 'post'], '/user/upload-photo', 'UsersCtrl@upload')->name('admin_user_upload');
Route::match(['get', 'post'], '/user/delete-photo', 'UsersCtrl@delete_photo')->name('admin_user_delete_photo');
Route::match(['get', 'post'], '/user/edit-photo-delete', 'UsersCtrl@edit_photo_delete')->name('admin_user_edit_photo_delete');
Route::match(['get', 'post'], '/user/logout', 'UsersCtrl@logout')->name('admin_logout');
    
//ROLES ROUTES  
Route::match(['get', 'post'], '/roles', 'RolesCtrl@index')->name('admin_role');
Route::match(['get', 'post'], '/role/add', 'RolesCtrl@add')->name('admin_add_role');
Route::match(['get', 'post'], '/role/edit/{id}', 'RolesCtrl@edit')->name('admin_edit_role');
Route::match(['get', 'post'], '/role/delete/{id}', 'RolesCtrl@delete')->name('admin_delete_role');
Route::match(['get', 'post'], '/role/default/{id}/{action}', 'RolesCtrl@default_toggle')->name('admin_default_role');

//SITE ROUTES
Route::match(['get', 'post'], '/configurations', 'OptionsCtrl@index')->name('admin_options');
Route::match(['get', 'post'], '/configuration/update-basic', 'OptionsCtrl@update_basic')->name('admin_options_update_basic');
Route::match(['get', 'post'], '/configuration/update-mail', 'OptionsCtrl@update_mail')->name('admin_options_update_mail');
Route::match(['get', 'post'], '/configuration/update-theme', 'OptionsCtrl@update_theme')->name('admin_options_update_theme');

    
});